# Require any additional compass plugins here.

# Set this to the root of your project when deployed:
http_path			= "/"

css_dir				= "public/stylesheets/"
sass_dir			= "_sources/stylesheets/scss"

javascripts_dir		= "public/javascripts"
javascripts_path	= "_sources/javascripts"

images_dir			= "public/images"
fonts_dir			= "public/fonts"

output_style = :compressed

# To enable relative paths to assets via compass helper functions. Uncomment:
# relative_assets = true

# To disable debugging comments that display the original location of your selectors. Uncomment:
# line_comments = false
color_output = false

preferred_syntax = :sass
